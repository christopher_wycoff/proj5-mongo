# Test BUTTONS

import flask
from flask import request, Response, redirect


app = flask.Flask(__name__)


@app.route("/")
@app.route("/index")
def index():
    print("TESTING BUTTONS CLICK ONE OF THE DATABASE BUTTONS TO TEST")
    
    assert True

    return flask.render_template('calc.html')



@app.route('/new', methods=['POST'])
def new():

	print("DATABASE ADD BUTTON WORKS")

	assert True

	response = Response(status=200)

	return response

@app.route("/display")
def _display_db():

	print("DISPLAY BUTTON WORKS")

	return redirect("/")



if __name__ == "__main__":
  
    app.run(port=5000, host="0.0.0.0")






